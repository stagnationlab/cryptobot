# CryptoBot

**Experimental cryptocurrency algotrader.**

## Commands
- `yarn start` to start the application in nodemon mode.
- `yarn dev` to start the compiler in watch mode.
- `yarn build` to build the sources once.
- `yarn lint` to run the linter.

## Initial setup
- copy `config/default.json` to `config/local.json`
- edit `config/local.json` to include the correct api keys etc (delete keys you don't change)

## Development workflow
- open two consoles side-by-side
- start `yarn dev` in one and `yarn start` in another to concurrency build in watch mode and reload rebuilt app.