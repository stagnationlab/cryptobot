import * as config from "config";
import { ICoinbaseConfig } from "../lib/exchange/coinbase";

export interface IExchangeConfig {
  coinbase: ICoinbaseConfig;
}

export interface IConfig {
  exchange: IExchangeConfig;
}

const configurations: IConfig = {
  exchange: config.get<IExchangeConfig>("exchange")
};

export default configurations;
