import { Currency, IExchange } from "./lib/exchange";
import { CoinbaseExchange } from "./lib/exchange/coinbase";
import config from "./services/config";

async function run() {
  const api: IExchange = new CoinbaseExchange(config.exchange.coinbase);
  const exchangeRates = await api.getExchangeRates(Currency.BTC);

  console.log("result", exchangeRates);
}

run()
  .then(() => {
    console.log("DONE");
  })
  .catch(e => {
    console.error(e);
  });
