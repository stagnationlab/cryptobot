import axios, { AxiosRequestConfig } from "axios";
import { createHmac } from "crypto";
import { Currency, IExchange, IExchangeRatesMap } from "../";

export enum HttpMethod {
  GET = "GET",
  POST = "POST"
}

export interface ICoinbaseConfig {
  baseUrl: string;
  key: string;
  secret: string;
}

export interface IExchangeRatesResponse {
  data: {
    currency: string;
    rates: {
      [x: string]: string;
    };
  };
}

// https://developers.coinbase.com/api/v2
export class CoinbaseExchange implements IExchange {
  constructor(private config: ICoinbaseConfig) {}

  public getName(): string {
    return "coinbase";
  }

  public async getExchangeRates(
    sourceCurrency: Currency
  ): Promise<IExchangeRatesMap> {
    const response = await this.request<IExchangeRatesResponse>(
      HttpMethod.GET,
      `/v2/exchange-rates?currency=${sourceCurrency}`
    );

    const allRates = response.data.rates;
    const supportedCurrencies = Object.keys(Currency) as Currency[];

    // filter to only return rates for supported currencies
    return supportedCurrencies.reduce<Partial<IExchangeRatesMap>>(
      (filteredRates, supportedCurrency) => {
        filteredRates[supportedCurrency] = parseFloat(
          allRates[supportedCurrency]
        );

        return filteredRates;
      },
      {}
    );
  }

  private async request<T = any>(method: HttpMethod, path: string): Promise<T> {
    const url = `${this.config.baseUrl}${path}`;
    const timestamp = Math.floor(Date.now() / 1000);
    const body = ""; // TODO: make argument
    const message = timestamp + method + path + body;
    const signature = createHmac("sha256", this.config.secret)
      .update(message)
      .digest("hex");
    const headers = {
      "CB-ACCESS-SIGN": signature,
      "CB-ACCESS-TIMESTAMP": timestamp,
      "CB-ACCESS-KEY": this.config.key,
      "CB-VERSION": "2015-07-22"
    };
    const requestConfig: AxiosRequestConfig = {
      method,
      url,
      headers
    };

    const response = await axios.request<T>(requestConfig);

    if (response.status !== 200) {
      throw new Error(
        `${method} request to ${url} failed with response ${
          response.status
        } (${response.statusText || "n/a"})`
      );
    }

    return response.data;
  }
}
