export enum Currency {
  BTC = "BTC",
  USD = "USD",
  EUR = "EUR"
}

export type CurrencyNames = keyof typeof Currency;

export type IExchangeRatesMap = { [k in CurrencyNames]?: number };

export interface IExchange {
  getName(): string;
  getExchangeRates(sourceCurrency: Currency): Promise<IExchangeRatesMap>;
}
